At least 4 GUIs you can start working on:
 
(1) User interface before calling R model-building program. The interface should allow users to enter the data set location, the value of parameters, the Determine variable and  Non-Determine variables, and the model type (classification or regression). The interface should provide a button to trigger an R-program and build a model.
 
(2) User interface before applying a new test sample to a previous built model. The test sample may come from a file or may be inputted by user manually. In the latter case, the interface should have multiple fields to collect values on each variable. After all inputs are available, the test sample should be fed into an available model and generate a predictive value.
 
(3) User interface to show the result of the newly built model which includes statistics measurements, list of the predictive values, and error ratio.
 
(4) User interface to show the predictive results after applying a previous built model for the given testing samples.
